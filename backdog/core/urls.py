from django.contrib import admin
from django.urls import path

from .views import (HomePageView, OrganizationalRoleCreate,
                    # OrganizationalTeamList, OrganizationalTeamCreate
                    OrganizationalRoleDelete,
                    OrganizationalRoleList, OrganizationalRoleUpdate, OrganizationCreate,
                    OrganizationDelete, OrganizationList, OrganizationUpdate, ProjectCreate,
                    ProjectList, ProjectTeamCreate, ProjectTeamList, ProjectTeamUpdate,
                    ProjectUpdate, TeamMemberCreate, TeamMemberList, TeamMemberUpdate,
                    TeamMemberDelete, TeamMembershipCreate, TeamMembershipList,
                    TeamPurposeCreate, TeamPurposeList, OrganizationDelete, ProjectTeamDelete)

urlpatterns = [
    path('', HomePageView.as_view(), name='home_page'),

    path('project/', ProjectList.as_view(), name='project_list'),
    path('organization/', OrganizationList.as_view(), name='organization_list'),
    path('organizational_role/', OrganizationalRoleList.as_view(), name='organizational_role_list'),
    path('project_team/', ProjectTeamList.as_view(), name='project_team_list'),
    # path('organizational_team/', OrganizationalTeamList.as_view(), name='organizational_team_list'),
    path('team_member/', TeamMemberList.as_view(), name='team_member_list'),
    path('team_purpose/', TeamPurposeList.as_view(), name='team_purpose_list'),
    path('team_membership/', TeamMembershipList.as_view(), name='team_membership_list'),

    path('project/add/', ProjectCreate.as_view(), name='project_add'),
    path('organization/add/', OrganizationCreate.as_view(), name='organization_add'),
    path('organizational_role/add/', OrganizationalRoleCreate.as_view(), name='organizational_role_add'),
    path('project_team/add/', ProjectTeamCreate.as_view(), name='project_team_add'),
    # path('organizational_team/add', OrganizationalTeamCreate.as_view(), name='organizational_team_add'),
    path('team_member/add', TeamMemberCreate.as_view(), name='team_member_add'),
    path('team_purpose/add/', TeamPurposeCreate.as_view(), name='team_purpose_add'),
    path('team_membership/add', TeamMembershipCreate.as_view(), name='team_membership_add'),

    path('organization/<int:pk>/update/', OrganizationUpdate.as_view(), name='organization_update'),
    path('project/<int:pk>/update/', ProjectUpdate.as_view(), name='project_update'),
    path('team_member/<int:pk>/update/', TeamMemberUpdate.as_view(), name='team_member_update'),
    path('projectteam/<int:pk>/update/', ProjectTeamUpdate.as_view(), name='projectteam_update'),
    path('organizational_role/<int:pk>/update/', OrganizationalRoleUpdate.as_view(), name='organizational_role_update'),

    path('organization/<int:pk>/delete/', OrganizationDelete.as_view(), name='organization_delete'),
    path('team_member/<int:pk>/delete/', TeamMemberDelete.as_view(), name='team_member_delete'),
    path('projectteam/<int:pk>/delete/', ProjectTeamDelete.as_view(), name='projectteam_delete'),
    path('organizational_role/<int:pk>/delete/', OrganizationalRoleDelete.as_view(), name='organizational_role_delete'),
]
